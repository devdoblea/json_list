import 'package:flutter/material.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';

void main() => runApp(JsonList());

class JsonList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Listas con JSON',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        ),
      home: MyHomePage(title: 'Usuarios'),
      );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  // con esta funcion llamo un JSON en internet pero le digo a flutter
  // que debe esperar a que se lea todo el JSON para dar mostrar los resultados
  Future<List<User>> _getUsers() async {

    var data = await http.get("http://www.json-generator.com/api/json/get/ckZPMkXemq?indent=2");

    // Con esta funcion decodifico el JSON  que viene en el body de la pagina de internet
    var parsedJson = json.decode(data.body);

    // inicializo como una lista el valor de User para poder mostrarlo
    List<User> users = [];

    //recorreo el array para llenar variables con los datos traidos
    for(var u in parsedJson) {

      User user = User(u["index"], u["about"], u["name"], u["email"], u["picture"], u["greeting"], u["favoriteFruit"], u["id"]);

      users.add(user);

    }

    print(users.length); // Imprimo por consola el valor length de users

    // Siempre se debe retornar un valor 
    // en este tipo de funciones
    return users;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ), //appBar
      body: Container(
        child: FutureBuilder(
          future: _getUsers(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            // si no hay datos que vengan en el snapshot que se hizo 
            //entonces aparecerá la palabra "Cargando..."
              if(snapshot.data == null) {
                return Container(
                  child: Center(
                    child: Text("Cargando..."),
                    ),
                );
              } else {
                return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      leading: CircleAvatar(
                        backgroundImage: NetworkImage(
                          snapshot.data[index].picture,
                        ),
                      ),
                      title: Text(snapshot.data[index].name),
                      subtitle: Text(snapshot.data[index].email),
                      onTap: () {
                        Navigator.push(context,
                          new MaterialPageRoute(builder: (context) => DetailPage(snapshot.data[index]))
                        );
                      }
                    );
                  }
                );
              }
          }
        ),//futureBuilder
      ), //Container
    );//Scaffold
  }
}

class DetailPage extends StatelessWidget {

  final User user;
  
  DetailPage(this.user);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(user.name)
      ),
      body: Center(
        child: Container(
          child: Text(
            user.name,
            textDirection: TextDirection.ltr,
            style: TextStyle(
              fontSize: 32,
              color: Colors.black87,
            ),
          ),
        ),
      ),
    );
  }
}

class User {
  final int index;
  final String about;
  final String name;
  final String email;
  final String picture;
  final String greeting;
  final String favoriteFruit;
  final String id;

  User(this.index, this.about, this.name, this.email, this.picture, this.greeting, this.favoriteFruit, this.id);

}