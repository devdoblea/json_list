Mostrar una lista de datos extraidos de un archivo en formato json.

Facil y sencillo. Se hace uso del Async y el await para esperar que los 
datos lleguen bien y completos antes de mostrarlos en pantalla.

El archivo Json fue extraido de aquí:
http://www.json-generator.com/api/json/get/ckZPMkXemq?indent=2

Así fue como quedó:
![Pantalla Inicial](assets/images/Screenshot_2019-11-25-16-47-11.png)

# json_list_app

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
